package com.adionisio.weatherapp.data

import android.util.Log
import java.net.URL

/**
 * Created by Alejandro on 04/09/2018.
 */
class Request(private val url: String){
    fun run(){
        val forecastJsonStr = URL(url).readText()
        Log.d(javaClass.simpleName, forecastJsonStr)
    }
}