package com.adionisio.weatherapp.domain.commands

import com.adionisio.weatherapp.data.ForecastRequest
import com.adionisio.weatherapp.domain.mappers.ForecastDataMapper
import com.adionisio.weatherapp.domain.model.ForecastList

/**
 * Created by Alejandro on 05/09/2018.
 */
class RequestForecastCommand( private  val zipcode: String): Command<ForecastList>{
    override fun execute(): ForecastList {
        val forecastRequest = ForecastRequest(zipcode)
        return ForecastDataMapper().convertFromDataModel(forecastRequest.execute())
    }
}