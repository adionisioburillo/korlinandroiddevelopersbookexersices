package com.adionisio.weatherapp.domain.commands

/**
 * Created by Alejandro on 04/09/2018.
 */
public interface Command<out T>{
    fun execute():T
}